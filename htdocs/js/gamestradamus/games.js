require([
    "dojo/query",
    "atemi/util/TableSort",
    "dojo/NodeList-dom",
    "dojo/domReady!"
], function(query, TableSort) {

    new TableSort(
        'game_list', [0, 0], [
            TableSort.textContent,
            TableSort.parseFloat,
            TableSort.parseFloat,
            TableSort.parseFloat,
            function(td) {
                var rating = query('div.rating', td)[0];
                return parseInt(rating.getAttribute('data-rating'));
            } 
        ]
    ); 
})
