import json
import os.path
import random
import string

from fabric.api import (
    cd,
    env,
    execute,
    get,
    hosts,
    lcd,
    local,
    prefix,
    prompt,
    put,
    roles,
    run,
    settings,
    sudo,
    reboot,
)
from fabric import colors
from fabric.contrib.console import confirm
from fabric.contrib.project import rsync_project
from fabric.contrib import files
from commands import getoutput

from baste import (
    DiffCommand,
    Git,
    local_changes,
    Mercurial,
    OrderedDict,
    pay_attention_confirm,
    PgLoadPlain,
    PgRestore,
    PgShell,
    project_relative,
    python_dependency,
    RsyncDeployment,
    StatusCommand,
    Subversion,
    UbuntuPgCreateDbAndUser,
)


#------------------------------------------------------------------------------
PYTHON_VERSION = "python2.7"
BOOST_1_52_0 = "http://downloads.sourceforge.net/project/boost/boost/1.52.0/boost_1_52_0.tar.bz2?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fboost%2Ffiles%2Fboost%2F1.52.0%2F&ts=1361412171&use_mirror=hivelocity"
RABBITMQ_DOWNLOAD = "http://www.rabbitmq.com/releases/rabbitmq-server/v3.0.2/rabbitmq-server-generic-unix-3.0.2.tar.gz"


DB_PORT = 5432
RABBIT_MQ_PORT = 5672
CRYSTALBALL_PORT = 8088
default_user = 'gauge'

#------------------------------------------------------------------------------
class Machine(object):
    #--------------------------------------------------------------------------
    def __init__(self, hostname, public_ip, private_ip):
        self.hostname = hostname
        self.public_ip = public_ip
        self.private_ip = private_ip

    #--------------------------------------------------------------------------
    def connection_host(self):
        return"%s@%s" % (default_user, self.public_ip)

    #--------------------------------------------------------------------------
    def __unicode__(self):
        return u"Machine(%s)" % self.hostname
    __repr__ = __unicode__

#------------------------------------------------------------------------------
machine_configuration = None
machines = {}
machine_roles = {}
env.roledefs = {}

#------------------------------------------------------------------------------
def add_machine(name, machine, role):
    """Overwrite the current machine with the new information.

    This will ensure that the machine information is not duplicated.
    """
    global machine_configuration
    machine_configuration['machines'][name] = machine
    machine_configuration['roles'][role].append(name)
    # Ensure uniqueness
    machine_configuration['roles'][role] = list(set(machine_configuration['roles'][role]))

#------------------------------------------------------------------------------
def write_machine_configuration():
    content = json.dumps(
            machine_configuration,
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
        )
    open(project_relative("machines.json"), "w").write(content)

#------------------------------------------------------------------------------
def read_machine_configuration():
    global machine_configuration, machines, machine_roles
    machines = {}
    machine_roles = {}
    machine_configuration = json.loads(open(project_relative("machines.json"), "r").read())
    for name, params in machine_configuration['machines'].iteritems():
        machines[name] = Machine(**params)
    for role, hostnames in machine_configuration['roles'].iteritems():
        machine_roles[role] = []
        for hostname in hostnames:
            machine_roles[role].append(machines[hostname])

    env.roledefs = {
        'web': [machine.connection_host() for machine in machine_roles['web']],
        'db': [machine.connection_host() for machine in machine_roles['db']],
        'crystalball': [machine.connection_host() for machine in machine_roles['crystalball']],
        'worker': [machine.connection_host() for machine in machine_roles['worker']],
    }

# Always read the machine configuration as soon as we run.
read_machine_configuration()


#------------------------------------------------------------------------------
python_repos = OrderedDict([(repo.name, repo) for repo in [

    Mercurial("geniusql", "ssh://hg@bitbucket.org/strabs/geniusql"),
    Mercurial("dejavu", "ssh://hg@bitbucket.org/strabs/dejavu"),
    Mercurial("pyrqlate", "ssh://hg@bitbucket.org/lakin.wecker/pyrqlate"),
    Git('linode', 'https://github.com/tjfontaine/linode-python.git'),
]])
static_file_repos = OrderedDict([(repo.name, repo) for repo in [
    Subversion("htdocs/js/dojo", "http://svn.dojotoolkit.org/src/tags/release-1.8.4/dojo"),
    Subversion("htdocs/js/util", "http://svn.dojotoolkit.org/src/tags/release-1.8.4/util"),
]])
all_repos = python_repos.copy()
all_repos.update(static_file_repos)
all_repos['baste'] = Mercurial("env/src/baste", "ssh://hg@bitbucket.org/lakin.wecker/baste")
all_repos['container'] = Mercurial(".", "redacted")

#-------------------------------------------------------------------------------
def update():
    """
    Update the all of the dependencies to their latest states.
    """
    # Static Media
    local("mkdir -p htdocs")
    local("mkdir -p var/tmp")
    local("mkdir -p var/lib/hydro")
    local("mkdir -p var/log/hydro")

    for repo in all_repos.values():
        repo.update()

    for repo in python_repos.values():
        python_dependency(repo.name, PYTHON_VERSION)
    python_dependency("gamestradamus", PYTHON_VERSION)

    #local("pip install -r requirements.txt --quiet")

up = update # defines up as the shortcut for update


#-------------------------------------------------------------------------------
st = status = StatusCommand(all_repos)

#-------------------------------------------------------------------------------
diff = DiffCommand(all_repos)

#-------------------------------------------------------------------------------
def dbshell():
    PgShell('gamestradamus', 'gamestradamus')()

#-------------------------------------------------------------------------------
def test():
    """Run the gamestradamus tests."""
    local("nosetests -sv gamestradamus")

#-------------------------------------------------------------------------------
def getdbparams():
    local_dir = os.path.join(os.getcwd(), os.path.dirname(__file__))
    from cherrypy.lib import reprconf
    config_file = os.path.join(local_dir, 'gamestradamus', 'deploy.conf')
    config = reprconf.as_dict(config_file)
    db_conf = config.get('global', {}).get('hydro.database.config', {})

    parts = db_conf['connections.Connect'].split(" ")
    params = {}
    for part in parts:
        key, value = part.split("=")
        params[key] = value
    return params

#-------------------------------------------------------------------------------
def printdbpw():
    password = getdbparams()['password']
    print colors.green("Password for the new role: ") + colors.blue(password)

#-------------------------------------------------------------------------------
def createdb():
    """Creates the db for gamestradamus."""
    if confirm(colors.red('Print out the db password?')):
        execute(printdbpw)

    UbuntuPgCreateDbAndUser('gamestradamus', 'gamestradamus')()

#-------------------------------------------------------------------------------
def createtestdb():
    """Creates the test db for gamestradamus tests."""
    print(
        colors.green("Look up the password to set for the user with: ") +\
        colors.blue("cat gamestradamus/tests/__init__.py | grep \"DB_PASSWORD =\"")
        )
    if confirm(colors.red("You should only run this when others aren't looking over your shoulder. Run the command??")):
        local("cat gamestradamus/tests/__init__.py | grep \"DB_PASSWORD =\"")
    UbuntuPgCreateDbAndUser('gamestradamus', 'gamestradamus')()

#-------------------------------------------------------------------------------
def migratedb():
    local("python gamestradamus/setupdb.py --upgrade")

#-------------------------------------------------------------------------------
def runserver(run_crystallball_server="no"):
    crystalball_var = project_relative("var/lib/crystalball")
    local("mkdir -p %s" % (crystalball_var,))
    rabbitmq_start = project_relative("deps/rabbitmq_server-3.0.2/sbin/rabbitmq-server")
    rabbitmq_ctl = project_relative("deps/rabbitmq_server-3.0.2/sbin/rabbitmqctl")
    local("%s -detached" % rabbitmq_start)

    hydro_conf = project_relative("gamestradamus/dev.conf")

    try:
        server_py = project_relative("gamestradamus/crystalball/server.py")
        server_conf = project_relative("gamestradamus/crystalball/server.conf")
        pid_file = project_relative("var/lib/crystalball/crystalball-1.pid")
        socket_port = "8088"
        socket_host = "0.0.0.0"
        if run_crystallball_server == "yes":
            with prefix('export LD_LIBRARY_PATH=`pwd`/cpp/boost_1_52_0/stage/lib'):
                local("python %s --daemonize --socket-port=%s --socket-host=%s --config=%s" % (
                    server_py,
                    socket_port,
                    socket_host,
                    server_conf,
                ))
        try:
            local("celery multi start w1 -Q welcome,premium,plebs,background -A gamestradamus.taskqueue -l info --concurrency=4")
            try:
                local("python hydro/hydro/index.py --environment=development --socket-host=0.0.0.0 --config=%s" % hydro_conf)
            finally:
                local("celery multi stop w1 -A gamestradamus.taskqueue -l info")
        finally:
            if run_crystallball_server == "yes":
                local("kill `cat %s`" % (pid_file,))
    finally:
        local("%s stop" % rabbitmq_ctl)

#-------------------------------------------------------------------------------
def checksyntax():
    gamestradamus = project_relative("htdocs/js/gamestradamus")
    command = 'find %s -name "*.js"  | grep -v ".jinja.js"' % (gamestradamus,)
    gamestradamus_files = getoutput(command).split("\n")

    atemi = project_relative("htdocs/js/lib/atemi")
    command = 'find %s -name "*.js"  | grep -v ".jinja.js"' % (atemi,)
    atemi_files = getoutput(command).split("\n")

    for file in atemi_files + gamestradamus_files:
        command = "java -jar compiler.jar --js_output_file /dev/null --js %s" % file
        print command
        output = getoutput(command)
        if output:
            print output

#-------------------------------------------------------------------------------
def build():
    build_sh = project_relative("htdocs/js/util/buildscripts/build.sh")
    build_sh_location = project_relative("htdocs/js/util/buildscripts/")
    profile_js = project_relative("htdocs/js/gamestradamus/gamestradamus.profile.js")
    dojo_config = project_relative("htdocs/js/config.js")
    build_config = project_relative("htdocs/js/release-build/config.js")
    command = "%s  --profile %s --dojoConfig %s" % (build_sh, profile_js, dojo_config)
    with lcd(build_sh_location):
        local(command)
    local("cp %s %s" % (dojo_config, build_config))

    def update_build_version(file):
        new_version = "".join([random.choice(string.letters + string.digits) for x in range(6)])
        print colors.green("Updating version number in %s: " % file),
        lines = []
        with open(file, "r") as fd:
            for line in fd.readlines():
                if line.strip().lower().startswith("gauge.build_version = "):
                    old_version = line.replace("gauge.build_version = \"", "").replace("\n", "").replace('"', "")
                    print colors.yellow("%s -> %s" % (old_version, new_version))
                    lines.append("gauge.build_version = \"%s\"\n" % new_version)
                else:
                    lines.append(line)
        with open(file, "w") as fd:
            fd.writelines(lines)
    update_build_version(project_relative("gamestradamus/deploy.conf"))
    update_build_version(project_relative("gamestradamus/dev.conf"))
    update_build_version(project_relative("gamestradamus/staging.conf"))

#-------------------------------------------------------------------------------
def install_deps():
    local("mkdir -p deps")
    print colors.green("Your SUDO password is needed next")
    local("sudo apt-get install erlang")
    rabbitmq_file_target = project_relative("deps/rabbitmq.tar.gz")
    local("wget -O \"%s\" \"%s\"" % (rabbitmq_file_target, RABBITMQ_DOWNLOAD))
    with lcd(project_relative("deps")):
        local("tar -zxf %s" % (rabbitmq_file_target,))

#-------------------------------------------------------------------------------
def install_boost():
    boost_file_target = project_relative("cpp/boost_1_52_0.tar.bz2")
    analyze_dir = project_relative("cpp")
    unzipped_dir = project_relative("cpp/boost_1_52_0")
    local("wget -O \"%s\" \"%s\"" % (boost_file_target, BOOST_1_52_0))
    with lcd(analyze_dir):
        local("tar -jxvf %s" % (boost_file_target,))
    local("rm %s" % boost_file_target)
    with lcd(unzipped_dir):
        local("./bootstrap.sh")
        local("./b2")

#-------------------------------------------------------------------------------
def make():
    with lcd(project_relative("cpp")):
        local("make")

#-------------------------------------------------------------------------------
def make_clean():
    with lcd(project_relative("cpp")):
        local("make clean")

#-------------------------------------------------------------------------------
def gator_runserver():
    with prefix('export LD_LIBRARY_PATH=`pwd`/cpp/boost_1_52_0/stage/lib'):
        local("python hydro/hydro/index.py --socket-port=8088 --socket-host=0.0.0.0 --config=steamadvisor/gator.conf")
rungator = gator_runserver

#-------------------------------------------------------------------------------
def summon():
    balls = project_relative("data/crystalball")
    config = project_relative("gamestradamus/dev.conf")
    with prefix('export LD_LIBRARY_PATH=`pwd`/cpp/boost_1_52_0/stage/lib'):
        local("mkdir -p %s" % balls)
        createindexes_py = project_relative("gamestradamus/bin/createindexes.py")
        local("python %s --directory %s --config %s" % (createindexes_py, balls, config))

#-------------------------------------------------------------------------------
def update_game_statistics():
    with prefix('export LD_LIBRARY_PATH=`pwd`/cpp/boost_1_52_0/stage/lib'):
        local("python %s" % (project_relative("gamestradamus/bin/update_game_statistics.py"),))


#-------------------------------------------------------------------------------
@roles('db')
def latestdb():
    DB_NAME = "gamestradamus"
    DB_USER = "gamestradamus"
    if confirm(colors.red("This will overwrite local data, are you sure?")):
        data_dir = project_relative("data")
        local("mkdir -p %s" % data_dir)

        remote_db = '/var/backups/gamestradamus-sql/hourly/latest.sql.bz2'
        local_db = project_relative("data/latest.sql.bz2")
        if confirm(colors.red("Grab a new copy?")):
            print colors.green("Password for gauge user is in: ") + colors.blue("redacted")
            local_db = get(remote_db, local_db)[0]
        with settings(warn_only=True):
            print colors.green("This is the database password")
            PgLoadPlain(local_db, DB_NAME, DB_USER)()

#-------------------------------------------------------------------------------
def configure_postgresql_master(listen_addresses):
    from juggernaut.ubuntu.lts1204 import postgresql
    postgresql.install()
    postgresql.configure(listen_addresses=listen_addresses)

    # NOTE: using 0.0.0.0/0 technically opens this up to ALL ips,
    #       but our firewall will limit those ips, so it should be fine.
    postgresql.configure_hba(
            "hostssl gamestradamus gamestradamus 0.0.0.0/0 md5"
        )
    params = getdbparams()
    if confirm("Print the password?"):
        print(colors.green(params['password']))
    postgresql.create_user(username=params['user'], dropuser=False)
    postgresql.create_db(dbname=params['dbname'], owner=params['user'], dropdb=False)
    postgresql.restart()


#-------------------------------------------------------------------------------
def configure_pg_backups():
    # Make the backup directory
    sudo("mkdir -p /var/backups/gamestradamus-sql/")
    sudo("chown gauge:gauge /var/backups/gamestradamus-sql/")
    password = getdbparams()['password']

    # Setup pgpass access
    pgpass = "*.*:gamestradamus:gamestradamus:%s" % password
    files.append("/home/gauge/.pgpass", pgpass)
    run("chmod 0600 /home/gauge/.pgpass")

    # Setup the
    crontab_source = "/var/www/gaugepowered.com/current/sysadmin/backup-crontab"
    crontab_target = "/etc/cron.d/postgresql-backup"
    sudo("cp %s %s" % (crontab_source, crontab_target))

#-------------------------------------------------------------------------------
def get_machines_with_db_access():
    """Returns the set of ips that should have access to the postgresql server."""
    return set(machine_roles['web'] + machine_roles['db'] + machine_roles['worker'] + machine_roles['crystalball'] + machine_roles['staging'])

#-------------------------------------------------------------------------------
def get_machines_with_rabbitmq_access():
    """Returns the set of ips that should have access to the rabbit mq server."""
    return set(machine_roles['web'] + machine_roles['worker'])

#-------------------------------------------------------------------------------
def get_machines_with_crystalball_access():
    """Returns the set of ips that should have access to the postgresql server."""
    return set(machine_roles['worker'])


#-------------------------------------------------------------------------------
def configure_firewall_db(server, install=True):
    """The database firewall will allow ssh, and connections to the db port
    from only the appropriate ips
    """
    from juggernaut.ubuntu.lts1204 import firewall

    if install:
        firewall.install()
    firewall.clear()

    # On the public
    with firewall.RulesFor(destination_ip=server.public_ip) as fw:
        fw.allow_established()
        fw.allow_ssh()
        fw.allow_ping()

    with firewall.RulesFor(in_interface="lo") as fw:
        fw.allow_incoming()

    firewall.allow_ping(destination_ip=server.private_ip)
    firewall.allow_established(in_interface="eth0:0")
    firewall.allow_outgoing(out_interface="eth0:0")

    # The private IP will allow communications from known host
    # to the database and rabbitmq
    with firewall.RulesFor(destination_ip=server.private_ip) as fw:
        fw.allow_established()
        # This one always requires a particular interface.
        fw.allow_ping()
        for machine in get_machines_with_db_access():
            firewall.allow_incoming(destination_port=DB_PORT, source_ip=machine.private_ip)
        for machine in get_machines_with_rabbitmq_access():
            firewall.allow_incoming(destination_port=RABBIT_MQ_PORT, source_ip=machine.private_ip)

        # allow the load balancer to access our local webs.
        fw.allow_incoming(destination_port="80", source_ip=machines['load1'].private_ip)

    # Otherwise block all incoming on all interfaces.
    firewall.block_all_incoming()
    firewall.persist()


#-------------------------------------------------------------------------------
def configure_firewall_web1():
    machine = machines['web1']
    target_ips = [default_user + '@' + machine.public_ip]
    execute(configure_firewall_web, server=machine, hosts=target_ips)

#-------------------------------------------------------------------------------
def configure_firewall_web(server, install=True):
    """The web firewall will allow ssh, and connections to the web ports
    from only the appropriate ips
    """
    from juggernaut.ubuntu.lts1204 import firewall

    if install:
        firewall.install()
    firewall.clear()

    # On the public
    with firewall.RulesFor(destination_ip=server.public_ip) as fw:
        fw.allow_established()
        fw.allow_ssh()
        fw.allow_ping()

    with firewall.RulesFor(in_interface="lo") as fw:
        fw.allow_incoming()

    firewall.allow_ping(destination_ip=server.private_ip)
    firewall.allow_established(in_interface="eth0:0")
    firewall.allow_outgoing(out_interface="eth0:0")

    # The private IP will allow communications from known host
    # to the database and rabbitmq
    with firewall.RulesFor(destination_ip=server.private_ip) as fw:
        fw.allow_established()
        # This one always requires a particular interface.
        fw.allow_ping()
        # allow the load balancer to access our local webs only on the private_ip address
        fw.allow_incoming(destination_port="80", source_ip=machines['load1'].private_ip)

    # Otherwise block all incoming on all interfaces.
    firewall.block_all_incoming()
    firewall.persist()

#-------------------------------------------------------------------------------
def configure_firewall_db1():
    db1 = machines['db1']
    target_ips = [default_user + '@' + db1.public_ip]
    execute(configure_firewall_db, server=machines['db1'], hosts=target_ips)

#-------------------------------------------------------------------------------
def configure_service_loadbalancer(install=True):
    from juggernaut.ubuntu.lts1204 import nginx
    if install:
        nginx.install()
    load_balancer = project_relative("sysadmin/load.gaugepowered.com")
    with open(load_balancer, "r") as load_balancer_fd:
        configuration = load_balancer_fd.read()
    nginx.configure(
            name="load.gaugepowered.com",
            configuration=configuration,
        )
    nginx.reload()

#-------------------------------------------------------------------------------
def configure_service_web_loadbalancer(install=True):
    from juggernaut.ubuntu.lts1204 import nginx
    if install:
        nginx.install()
    load_balancer = project_relative("sysadmin/web.gaugepowered.com")
    with open(load_balancer, "r") as load_balancer_fd:
        configuration = load_balancer_fd.read()
    nginx.configure(
            name="web.gaugepowered.com",
            configuration=configuration,
        )
    nginx.reload()

#-------------------------------------------------------------------------------
def configure_service_staging_loadbalancer():
    from juggernaut.ubuntu.lts1204 import nginx
    nginx.install()
    load_balancer = project_relative("sysadmin/staging/load.gaugepowered.com")
    with open(load_balancer, "r") as load_balancer_fd:
        configuration = load_balancer_fd.read()
    nginx.configure(
            name="load.gaugepowered.com",
            configuration=configuration,
        )
    nginx.reload()

#-------------------------------------------------------------------------------
def install_dependencies_code():
    from juggernaut.ubuntu.lts1204 import python
    directories = [
            "/var/www/gaugepowered.com",
            "/var/log/gaugepowered",
            "/var/log/hydro",
        ]
    for directory in directories:
        sudo("mkdir -p %s" % directory)

        sudo("chown -R gauge:www-data %s" % directory)
        sudo("chmod -R ug+rw %s" % directory)

    python.install_python27()
    python.install_virtualenv()

    env = "/var/www/gaugepowered.com/env"
    sudo(
        'virtualenv %s -p python2.7 --no-site-packages --prompt="(gauge):"' % (
            env,
        )
    )
    sudo("apt-get update")
    # TODO: these depencies may not need to be global to all servers.
    sudo("apt-get install -yq postgresql-server-dev-9.1 libxml2-dev libxslt1-dev libpqxx3-dev libpqxx-3.1 mercurial")

#-------------------------------------------------------------------------------
def deploy_code(skip_confirm=False):
    # This isn't technically needed for all code deployments
    #search_so = project_relative("gamestradamus/crystalball/search.so")
    #if not os.path.exists(search_so):
        #print colors.red("Missing file: %s" % search_so)
        #print colors.red("Can't deploy without this file.")
        #return

    changes = local_changes(all_repos.values())
    if changes:
        print '\n'.join(changes)
        if not pay_attention_confirm(colors.red('You have uncommitted local changes, are you sure you want to deploy?')):
            return

    if not skip_confirm:
        if not confirm(colors.red('Are you sure you want to deploy to the live server?')):
            return

    for directory in (
        "/var/www/gaugepowered.com",
        "/var/www/gaugepowered.com/current/",
        "/var/www/gaugepowered.com/previous/",
    ):
        sudo("chown gauge:www-data %s" % directory)
        sudo("chmod ug+rw %s" % directory)

    RsyncDeployment(
        '/var/www/gaugepowered.com/',
        project_relative(".") + "/"
    )(exclude=['media', 'cpp/boost_1_52_0', 'data', 'deps', 'env', 'var'])

    # put the requirements script there and use it to update dependencies.
    requirements_txt = project_relative('requirements.txt')
    requirements_target = "/var/www/gaugepowered.com/requirements.txt"
    put(requirements_txt, requirements_target)
    #run("/var/www/gaugepowered.com/current/sysadmin/update-deps.sh")

    # Install symlinks for our own code into the virtual env
    dependencies = [
            "cream/cream/",
            "dejavu/dejavu/",
            "gamestradamus/",
            "geniusql/geniusql/",
            "hydro/hydro/",
            "jamaisvu/jamaisvu/",
            "jsonrpclib/",
            "plumbing/plumbing/",
            "pyrqlate/pyrqlate/",
            "toro/toro/",
        ]
    for library in dependencies:
        sudo("ln -sf /var/www/gaugepowered.com/current/%s /var/www/gaugepowered.com/env/lib/python2.7/site-packages/" % library)

    # update the symlink necessary for boost directory
    boost_remote = "/var/crystalball/boost_1_52_0/"
    boost_container = "/var/www/gaugepowered.com/current/cpp/"
    boost_target = "%s/boost_1_52_0" % (boost_container,)
    sudo("mkdir -p %s" % (boost_container))
    sudo("ln -sf %s %s" % (boost_remote, boost_target))


#-------------------------------------------------------------------------------
def deploy_crystalball():
    # install boost as a dependency so that we can use it appropriately.
    boost_local = project_relative("cpp/boost_1_52_0")
    boost_remote = "/var/crystalball/"

    sudo("mkdir -p %s" % boost_remote)
    sudo("chown -R gauge:www-data %s" % boost_remote)
    sudo("chmod -R g+rw %s" % boost_remote)
    rsync_project(remote_dir=boost_remote, local_dir=boost_local)


#-------------------------------------------------------------------------------
def configure_service_web():
    # This is necessary for these webs to run. Make sure the load balancer is
    # properly configured.
    execute(configure_service_web_loadbalancer, install=False)

    # Ensures that the webs are setup to run on boot, and start them
    for x in range(1, 5):
        remote_file_source = "/var/www/gaugepowered.com/current/sysadmin/gauge-%s.conf" % x
        remote_file_target = "/etc/init/gauge-%s.conf" % x
        sudo("cp %s %s" % (remote_file_source, remote_file_target))

    for x in range(1, 5):
        sudo("service gauge-%s restart" % x)

    sudo("chown -R gauge:www-data `ls -l /var/www/gaugepowered.com/current | awk '{print $11}'`")
    sudo("chmod -R ug+rw `ls -l /var/www/gaugepowered.com/current | awk '{print $11}'`")

#-------------------------------------------------------------------------------
def configure_service_web_staging():
    # Ensures that the webs are setup to run on boot, and start them
    for x in range(1, 5):
        remote_file_source = "/var/www/gaugepowered.com/current/sysadmin/staging/gauge-%s.conf" % x
        remote_file_target = "/etc/init/gauge-%s.conf" % x
        sudo("cp %s %s" % (remote_file_source, remote_file_target))

    for x in range(1, 5):
        sudo("service gauge-%s restart" % x)

    sudo("chown -R gauge:www-data /var/www/gaugepowered.com")
    sudo("chmod -R ug+rw  /var/www/gaugepowered.com")

#-------------------------------------------------------------------------------
def configure_service_spider():
    # Ensure that the spiders have their appropriate crontab entries.
    # Update the crontab

    # Copy from the deployed code rather than locally, as we always want to
    # match the crontab script with the deployed code.
    crontab_source = "/var/www/gaugepowered.com/current/sysadmin/spiders-crontab"
    crontab_target = "/etc/cron.d/gaugespiders"
    sudo("cp %s %s" % (crontab_source, crontab_target))

#-------------------------------------------------------------------------------
def configure_crontab_web():
    crontab_source = "/var/www/gaugepowered.com/current/sysadmin/web-crontab"
    crontab_target = "/etc/cron.d/web-crontab"
    sudo("cp %s %s" % (crontab_source, crontab_target))


#-------------------------------------------------------------------------------
def configure_firewall_loadbalancer(server):
    """The load firewall should allow:
        * HTTP for everyone
        * HTTPS for everyone
        * SSH for everyone (maybe only for us in the future?)
    """
    from juggernaut.ubuntu.lts1204 import firewall

    firewall.install()
    firewall.clear()
    # On the public
    with firewall.RulesFor(destination_ip=server.public_ip) as fw:
        fw.allow_established()
        fw.allow_ssh()
        fw.allow_ping()
        fw.allow_http()
        fw.allow_https()

    with firewall.RulesFor(in_interface="lo") as fw:
        fw.allow_established()
        fw.allow_incoming()

    firewall.allow_ping(destination_ip=server.private_ip)

    # Allow all outgoing on the internal network
    # This could perhaps be a bit more restrictive, but let's try this for now.
    with firewall.RulesFor(in_interface="eth0:0") as fw:
        fw.allow_established()
        for machine in get_machines_with_crystalball_access():
            firewall.allow_incoming(destination_port=CRYSTALBALL_PORT, source_ip=machine.private_ip)

    with firewall.RulesFor(out_interface="eth0:0") as fw:
        fw.allow_outgoing()

    firewall.allow_established()
    firewall.block_all_incoming()
    firewall.persist()

#-------------------------------------------------------------------------------
def configure_firewall_load1():
    execute(configure_firewall_loadbalancer, server=machines['load1'])

#-------------------------------------------------------------------------------
def configure_firewall_staging(server):
    """Configure the firewall for staging.
    """
    from juggernaut.ubuntu.lts1204 import firewall

    firewall.install()
    firewall.clear()
    # On the public
    with firewall.RulesFor(destination_ip=server.public_ip) as fw:
        fw.allow_established()
        fw.allow_ssh()
        fw.allow_ping()
        fw.allow_http()
        fw.allow_https()

    with firewall.RulesFor(in_interface="lo") as fw:
        fw.allow_established()
        fw.allow_incoming()

    firewall.allow_ping(destination_ip=server.private_ip)

    # Allow all outgoing on the internal network
    # This could perhaps be a bit more restrictive, but let's try this for now.
    with firewall.RulesFor(in_interface="eth0:0") as fw:
        fw.allow_established()

    with firewall.RulesFor(out_interface="eth0:0") as fw:
        fw.allow_outgoing()

    firewall.allow_established()
    firewall.block_all_incoming()
    firewall.persist()

#-------------------------------------------------------------------------------
def configure_firewall_worker(server):
    """The load firewall should allow:
        * SSH for everyone (maybe only for us in the future?)
    """
    from juggernaut.ubuntu.lts1204 import firewall

    firewall.install()
    firewall.clear()
    # On the public
    with firewall.RulesFor(destination_ip=server.public_ip) as fw:
        fw.allow_established()
        fw.allow_ssh()
        fw.allow_ping()

    with firewall.RulesFor(in_interface="lo") as fw:
        fw.allow_established()
        fw.allow_incoming()

    firewall.allow_ping(destination_ip=server.private_ip)

    # Allow all outgoing on the internal network
    # This could perhaps be a bit more restrictive, but let's try this for now.
    with firewall.RulesFor(in_interface="eth0:0") as fw:
        fw.allow_established()
    with firewall.RulesFor(out_interface="eth0:0") as fw:
        fw.allow_outgoing()

    firewall.allow_established()
    firewall.block_all_incoming()
    firewall.persist()

#-------------------------------------------------------------------------------
def configure_service_rabbitmq(listen_addresses):
    from juggernaut.ubuntu.lts1204 import rabbitmq
    rabbitmq.install()
    rabbitmq.configure(listen_addresses=listen_addresses)
    rabbitmq.reload()

#-------------------------------------------------------------------------------
def configure_service_redis():
    from juggernaut.ubuntu.lts1204 import redis
    redis.install()

#-------------------------------------------------------------------------------
def configure_service_celeryworkers():
    # This assumes that the code has been deployed to this machine.
    for x in range(1, 5):
        remote_file_source = "/var/www/gaugepowered.com/current/sysadmin/celery-worker-%s.conf" % x
        remote_file_target = "/etc/init/celery-worker-%s.conf" % x
        sudo("cp %s %s" % (remote_file_source, remote_file_target))

    # Update the crontab
    crontab_source = "/var/www/gaugepowered.com/current/sysadmin/celery-worker-crontab"
    crontab_target = "/etc/cron.d/celeryworker"
    sudo("cp %s %s" % (crontab_source, crontab_target))

    for x in range(1, 5):
        sudo("service celery-worker-%s restart" % x)

#-------------------------------------------------------------------------------
def configure_service_crystallballserver(install=True):
    # The crystalballs also have an nginx load balancer (internally)
    from juggernaut.ubuntu.lts1204 import nginx
    if install:
        nginx.install()
    load_balancer = project_relative("sysadmin/crystalball-loadbalancer")
    with open(load_balancer, "r") as load_balancer_fd:
        configuration = load_balancer_fd.read()
    nginx.configure(
            name="crystalball-loadbalancer",
            configuration=configuration,
        )
    nginx.reload()

    # Update the crontab
    crontab_source = "/var/www/gaugepowered.com/current/sysadmin/crystalball-crontab"
    crontab_target = "/etc/cron.d/gaugecrystallball"
    sudo("cp %s %s" % (crontab_source, crontab_target))

    # Do the same for each of the gauge-%d init scripts
    for x in range(1, 3):
        remote_file_source = "/var/www/gaugepowered.com/current/sysadmin/crystalball-%s.conf" % x
        remote_file_target = "/etc/init/crystalball-%s.conf" % x
        sudo("cp %s %s" % (remote_file_source, remote_file_target))

    for x in range(1, 3):
        sudo("service crystalball-%s restart" % x)

#-------------------------------------------------------------------------------
def provision_db1():
    """Configures the master database server
    """

    db1 = machines['db1']
    target_ips = [default_user + '@' + db1.public_ip]
    execute(configure_firewall_db, server=db1, hosts=target_ips)
    execute(
            configure_postgresql_master,
            listen_addresses=db1.private_ip,
            hosts=target_ips
        )

    execute(
            configure_service_rabbitmq,
            listen_addresses=db1.private_ip,
            hosts=target_ips
        )

    execute(install_dependencies_code, hosts=target_ips)

    execute(reboot, wait=60, hosts=target_ips)

    execute(deploy_db1, hosts=target_ips)



#-------------------------------------------------------------------------------
def provision_load1():
    """Configures a web server
    """
    hostname = "load1"
    load1 = machines['load1']
    target_ips = [default_user + '@' + load1.public_ip]
    execute(configure_firewall_loadbalancer, server=load1, hosts=target_ips)
    execute(configure_service_loadbalancer, install=True, hosts=target_ips)
    execute(install_dependencies_code, hosts=target_ips)

    execute(reboot, wait=60, hosts=target_ips)
    execute(deploy_load1, hosts=target_ips)

#-------------------------------------------------------------------------------
def provision_staging1():
    """Configures a web server
    """
    # First things first, let's update the db firewall to allow me to connect.
    db1 = machines['db1']
    target_ips = [default_user + '@' + db1.public_ip]
    execute(configure_firewall_db, server=db1, hosts=target_ips)

    hostname = "staging1"
    staging1 = machines['staging1']
    target_ips = [default_user + '@' + staging1.public_ip]

    # the webs need to use redis for their sessions.
    execute(configure_service_redis, hosts=target_ips)
    execute(configure_firewall_staging, server=staging1, hosts=target_ips)
    execute(configure_service_staging_loadbalancer, hosts=target_ips)
    execute(
            configure_service_rabbitmq,
            listen_addresses='127.0.0.1',
            hosts=target_ips
        )
    execute(install_dependencies_code, hosts=target_ips)

    execute(reboot, wait=60, hosts=target_ips)
    execute(deploy_staging1, hosts=target_ips)

#-------------------------------------------------------------------------------
def provision_worker(machine):
    """Configures a worker in our server pool
    """

    # First things first, let's update the db firewall to allow me to connect.
    db1 = machines['db1']
    target_ips = [default_user + '@' + db1.public_ip]
    execute(configure_firewall_db, server=db1, hosts=target_ips)

    load1 = machines['load1']
    target_ips = [default_user + '@' + load1.public_ip]
    execute(configure_firewall_loadbalancer, server=load1, hosts=target_ips)

    target_ips = [default_user + '@' + machine.public_ip]
    execute(configure_firewall_worker, server=machine, hosts=target_ips)
    execute(install_dependencies_code, hosts=target_ips)

    execute(deploy_worker, hosts=target_ips)

#-------------------------------------------------------------------------------
def provision_web(machine):
    """Configures a web in our server pool
    """

    # First things first, let's update the db firewall to allow me to connect.
    db1 = machines['db1']
    target_ips = [default_user + '@' + db1.public_ip]
    execute(configure_firewall_db, server=db1, hosts=target_ips)

    load1 = machines['load1']
    target_ips = [default_user + '@' + load1.public_ip]
    execute(configure_firewall_loadbalancer, server=load1, hosts=target_ips)

    target_ips = [default_user + '@' + machine.public_ip]

    # the webs need to use redis for their sessions.
    execute(configure_service_redis, hosts=target_ips)
    execute(configure_firewall_web, server=machine, hosts=target_ips)
    execute(configure_service_web_loadbalancer, hosts=target_ips)
    execute(install_dependencies_code, hosts=target_ips)

    execute(deploy_web, hosts=target_ips)

    print colors.yellow("Add these webs to the sysadmin/load.gaugepowered.com: %s" % (
        """
server %(ip)s:80;
""" % {'ip': machine.private_ip},
    ))

#-------------------------------------------------------------------------------
def deploy_db1():
    db1 = machines['db1']
    target_ips = [default_user + '@' + db1.public_ip]
    # Both the webs and spiders depend on the code
    execute(deploy_code, hosts=target_ips)

    execute(configure_service_web, hosts=target_ips)
    execute(configure_service_spider, hosts=target_ips)
    execute(configure_pg_backups, hosts=target_ips)

#-------------------------------------------------------------------------------
def deploy_load1():
    load1 = machines['load1']
    target_ips = [default_user + '@' + load1.public_ip]
    # Both the workers and crystall ball servers depend on the code
    execute(deploy_code, hosts=target_ips)
    execute(deploy_crystalball, hosts=target_ips)

    execute(configure_service_celeryworkers, hosts=target_ips)
    execute(configure_service_crystallballserver, install=True, hosts=target_ips)

#-------------------------------------------------------------------------------
def deploy_staging1():
    load1 = machines['staging1']
    target_ips = [default_user + '@' + load1.public_ip]
    # Both the workers and crystall ball servers depend on the code
    execute(deploy_code, hosts=target_ips)

    execute(configure_service_celeryworkers, hosts=target_ips)
    execute(configure_service_web_staging)

#-------------------------------------------------------------------------------
def deploy_worker():
    """Deploys code to one of the workers
    """
    execute(deploy_code)
    execute(configure_service_celeryworkers)

#-------------------------------------------------------------------------------
def deploy_web():
    """Deploys code to one of the workers
    """
    execute(deploy_code)
    execute(configure_service_web)
    execute(configure_crontab_web)

#-------------------------------------------------------------------------------
def deploy():
    if not pay_attention_confirm(colors.red('Are you sure you want to deploy to the live servers?')):
        return

    # The dbs, webs and workers all need the code deployed to them, but only do it once!
    code_hosts = list(set(env.roledefs['web'] + env.roledefs['worker'] + env.roledefs['db']))

    # The webs and workers depend on the code.
    execute(deploy_code, skip_confirm=True, hosts=code_hosts)
    execute(configure_service_web, hosts=env.roledefs['web'])

    # the crystalball servers depend on the crystalball code.
    execute(deploy_crystalball, hosts=env.roledefs['crystalball'])

    # Ensure that everything restarts
    execute(configure_service_celeryworkers, hosts=env.roledefs['worker'])
    execute(configure_service_crystallballserver, install=False, hosts=env.roledefs['crystalball'])
    execute(configure_service_spider, hosts=env.roledefs['db'])
    execute(configure_pg_backups, hosts=env.roledefs['db'])
    execute(configure_crontab_web, hosts=env.roledefs['web'])

    # Configure the loadbalancer server
    execute(configure_service_load1)


#-------------------------------------------------------------------------------
def configure_service_load1():
    load1 = machines['load1']
    target_ips = [default_user + '@' + load1.public_ip]
    execute(configure_service_loadbalancer, install=False, hosts=target_ips)

#-------------------------------------------------------------------------------
def configure_networking(hostname, fqdn, public_ip, private_ip):
    gateway = ".".join(public_ip.split(".")[:3] + ['1'])

    local('ssh-keygen -f "/home/lakin/.ssh/known_hosts" -R %s' % public_ip)
    print(colors.green("Configuring machine. root PW is in: redacted"))
    sudo("""echo "%s" > /etc/hostname""" % hostname)
    sudo("hostname -F /etc/hostname")
    host_line = "%s      %s        %s" % (
                public_ip, fqdn, hostname
            )
    files.append("/etc/hosts", host_line, use_sudo=True)
    interface = """
# The loopback interface
auto lo
iface lo inet loopback

# Configuration for eth0 and aliases

# This line ensures that the interface will be brought up during boot.
auto eth0 eth0:0

# eth0 - This is the main IP address that will be used for most outbound connections.
# The address, netmask and gateway are all necessary.
iface eth0 inet static
  address %(public_ip)s
  netmask 255.255.255.0
  gateway %(gateway)s

# eth0:0 - Private IPs have no gateway (they are not publicly routable) so all you need to
# specify is the address and netmask.
iface eth0:0 inet static
  address %(private_ip)s
  netmask 255.255.128.0
""" % {
        'public_ip': public_ip,
        'private_ip': private_ip,
        'gateway': gateway,
    }
    sudo("rm /etc/network/interfaces")
    files.append("/etc/network/interfaces", interface.split("\n"), use_sudo=True)
    sudo("/etc/init.d/networking restart")
    sudo("apt-get update")
    sudo("apt-get -yq remove isc-dhcp-client dhcp3-client dhcpcd")
    sudo("apt-get -yq install openssh-server")

    print(colors.green("Adding gauge user. PW is in: redacted"))
    sudo("adduser gauge")
    sudo("adduser gauge sudo")

#-------------------------------------------------------------------------------
def configure_apt_sources():
    sudo('sed -i -e "s/us.archive.ubuntu.com/mirror.pnl.gov/g" /etc/apt/sources.list')
    sudo("apt-get update")

#-------------------------------------------------------------------------------
def requests_create_request(url, fields, headers):
    return dict(url=url, fields=fields, headers=headers)

#-------------------------------------------------------------------------------
def make_request(request):
    import requests
    from types import MethodType
    response = requests.get(
            request['url'],
            params=request['fields'],
            headers=request['headers']
        )
    response.read = MethodType(lambda x: x.text, response)
    return response


#-------------------------------------------------------------------------------
def provision():
    """Configures a linode server into our base system.

    The base system will have the appropriate networking configured as well as
    having the appropriate root/gauge users setup, etc.
    """
    from linode import api as linode_api

    # Monkey patch them to use our version of requests
    linode_api.URLREQUEST = requests_create_request
    linode_api.URLOPEN = make_request

    # Root user is already setup. First step is setting up the appropriate
    # networking.
    api_key = prompt("What is the linode API Key for linode (Find it here: redacted )?")
    hostname = prompt("What is the hostname for the machine?")
    fqdn = '%s.gaugepowered.com' % hostname

    api = linode_api.Api(api_key)

    # Get machine details
    linode_label = "gauge-%s" % hostname
    print colors.green("Looking for gauge machine labeled: %s" % linode_label)
    target_machine = None
    all_machines = {}
    for machine in api.linode_list():
        all_machines[machine['LABEL']] = machine
        if machine['LABEL'] == linode_label:
            target_machine = machine
            break

    if not target_machine:
        print "Could not find linode machine with label: %s" % linode_label
        labels = [l for l in all_machines.keys() if l.startswith("gauge-")]
        print "Found following machines: " + colors.green("%s" % labels)
        linode_label = prompt("What is the linode label for the machine?")
        print colors.green("Looking for gauge machine labeled: %s" % linode_label)
        if linode_label not in all_machines:
            print "Could not find linode machine with label: %s" % linode_label
            return
        target_machine = all_machines[linode_label]

    linode_id = target_machine['LINODEID']
    public_ip = None
    private_ip = None
    for ip in api.linode_ip_list(linodeid=linode_id):
        if ip['ISPUBLIC'] == 0:
            private_ip = ip['IPADDRESS']
        else:
            public_ip = ip['IPADDRESS']

    if private_ip is None:
        print colors.red("%s needs a private ip!" % hostname)
        return
    target_ips = ['root@' + public_ip]
    execute(configure_apt_sources, hosts=target_ips)
    execute(
            configure_networking,
            hostname=hostname,
            fqdn=fqdn,
            public_ip=public_ip,
            private_ip=private_ip,
            hosts=target_ips
        )

    # Debuggin information
    # print("Public IP: " + colors.yellow(public_ip))
    # print("Private IP: " + colors.yellow(private_ip))
    # print("Fully Qualified Domain Name: " + colors.yellow(fqdn))

    machine_info = {
            "hostname": hostname,
            "public_ip": public_ip,
            "private_ip": private_ip,
        }

    types = [u'worker', u'load1', u'db1', 'web', 'staging1',]
    type = prompt("Which type of machine is this " + colors.blue("%s" % types))
    if type not in types:
        print "Invalid type chosen"
        return
    if type == 'load1':
        provision_load1()
    elif type == 'db1':
        provision_db1()
    elif type == 'staging1':
        add_machine(machine_info['hostname'], machine_info, 'staging')
        write_machine_configuration()
        read_machine_configuration()
        provision_staging1()
    elif type == 'worker':
        machine = Machine(**machine_info)
        add_machine(machine_info['hostname'], machine_info, 'worker')
        write_machine_configuration()
        provision_worker(machine)
    elif type == 'web':
        machine = Machine(**machine_info)
        add_machine(machine_info['hostname'], machine_info, 'web')
        write_machine_configuration()
        provision_web(machine)

